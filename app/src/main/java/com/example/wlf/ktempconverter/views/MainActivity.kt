package com.example.wlf.ktempconverter.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import android.widget.*

import com.example.wlf.ktempconverter.R
import com.example.wlf.ktempconverter.classes.Adapter
import com.example.wlf.ktempconverter.classes.Temperature

class MainActivity : AppCompatActivity()  /* CLIENT */
{
    private lateinit var inputValue      :EditText
    private lateinit var converted       : TextView
    private lateinit var formula         : TextView
    private lateinit var celciusRadio    : RadioButton
    private lateinit var fahrenheitRadio : RadioButton
    private lateinit var kelvinRadio     : RadioButton
    private lateinit var converterButton : Button
    private lateinit var clear           : Button
    private lateinit var linearLayout    : LinearLayout

    private val  tempAdapter = Adapter()

    override fun onCreate( savedInstanceState : Bundle? )
    {
        super.onCreate( savedInstanceState )
        setContentView( R.layout.activity_main )

        applyScreenFull()

        inputValue      = findViewById( R.id.valorTemp )
        converted       = findViewById( R.id.converted )
        formula         = findViewById( R.id.formula )
        celciusRadio    = findViewById( R.id.celciusRadio )
        fahrenheitRadio = findViewById( R.id.fahrenheitRadio )
        kelvinRadio     = findViewById( R.id.KelvinRadio )
        converterButton = findViewById( R.id.converterButton )
        clear           = findViewById( R.id.clear )
        linearLayout   = findViewById( R.id.linear_layout_main )

        converterButton.setOnClickListener { convert() }
        clear.setOnClickListener { clearFields() }

        clearFields()
    }

    private fun convert()
    {
        val temp: String = inputValue.text.toString()

        if ( temp.isNotEmpty() && temp != "0" )
        {
            val temp: Float = inputValue.text.toString().toFloat()

            when
            {
                fahrenheitRadio.isChecked ->
                {
                    converted.text = tempAdapter.viewTemperature( temp, 'f' ).toString()
                    viewFormula( temp, 'F', tempAdapter.viewTemperature( temp, 'f' ) )
                }

                kelvinRadio.isChecked ->
                {
                    converted.text = tempAdapter.viewTemperature( temp, 'k' ).toString()
                    viewFormula( temp, 'K', tempAdapter.viewTemperature( temp, 'k' ) )
                }
            }
        }

        else
        {
            showMSG( getString(R.string.please_type_a_temp) )
            inputValue.error = getString(R.string.type_a_valid_value)
        }
    }

    private fun viewFormula( w: Float, e: Char, b: Float ): String
    {
        formula.visibility = VISIBLE

        return when ( e )
        {
            'F' -> formula.setText( "Fórmula (${w}ºC × 9/5) + 32 = ${b}º$e" ).toString()

            'K' -> formula.setText( "Fórmula ${w}ºC + 273.15 = ${b}º$e" ).toString()

            else -> "0"
        }
    }

    private fun showMSG( msg: String ) = Snackbar.make( linearLayout, msg, Snackbar.LENGTH_LONG ).show()

    private fun clearFields()
    {
        formula.visibility = GONE
        formula.text = ""
        inputValue.setText( "" )
        converted.text = ""
    }

    private fun applyScreenFull()
    {
        window.addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON )
    }
}

