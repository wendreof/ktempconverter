package com.example.wlf.ktempconverter.classes

class Adapter : Temperature, ThermometricScale()  /* ADAPTER */
{
    //Request
    override fun viewTemperature( value: Float, s: Char ): Float
    {
        return when ( s )
        {
            'c' -> ThermometricScale().celsius( value )

            'f' -> ThermometricScale().fahrenheit( value )

            'k' -> ThermometricScale().kelvin( value )

            else ->  null!!.toFloat()
        }
    }
}


